#include "tcp_socket.hpp"

//服务端
//


int main(int argc,char* argv[])
{
  if(argc!=3){
    std::cout<<"nUsage:./tcp_ser 192.168.75.128 9000"<<std::endl;
    return -1;
  }
  std::string ip=argv[1];
  uint16_t  port=atoi(argv[2]);

  TcpSocket lsn_sock;  //实例化一个套接字结构对象
  //服务端
  //1、创建套接字
  assert(lsn_sock.Socket()!=false);

  //2、绑定地址
  assert(lsn_sock.Bind(ip,port));
  //3、开始监听
  assert(lsn_sock.Listen());
  while(1){
    //4、建立新连接
    TcpSocket new_sock;  //新建套接字，因为客户端连接可能不止一个，因此需要循环进行建立连接
    std::string cli_ip;
    uint16_t cli_port;
    bool ret=lsn_sock.Accept(&new_sock,&cli_ip,&cli_port);
    if(ret==false){
      //一次客户端的新建连接失败，则继续处理下一个客户端请求
      continue;
    }
    std::cout<<"new client: "<<cli_ip<<" : "<<cli_port<<std::endl;

    while(1){
      //5、收发数据
      std::string buf;
      ret = new_sock.Recv(&buf);
      if(ret==false){
        //接受失败
        new_sock.Close();   //说明连接不成立----关闭套接字
        //continue;
        break;
      }
      std::cout<<cli_ip<<" : "<<cli_port<<"say : "<<buf<<std::endl;

      buf.clear();
      std::cout<<"server say : ";
      fflush(stdout);
      std::cin>>buf;
      ret = new_sock.Send(buf);
      if(ret==false){
        new_sock.Close();
        //continue;  //发送数据失败，关闭套接字
        break;
      }
    }
  }
  //6、关闭套接字
  lsn_sock.Close();  

  return 0;
}
