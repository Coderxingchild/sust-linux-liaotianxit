#include "tcp_socket.hpp"

//客户端
//

int main(int argc,char* argv[])
{
  if(argc!=3){
    std::cout<<"nUsage:./tcp_cli.c 192.168.75.128 9000"<<std::endl;
    return -1;
  }
  std::string ip=argv[1];
  uint16_t port=std::stoi(argv[2]);

  TcpSocket cli_sock;

  //1、创建套接字
  assert(cli_sock.Socket()!=false);
  //2、绑定地址（客户端不建议）
  
  //3、请求连接
  assert(cli_sock.Connect(ip,port));
  while(1){
  //4、收发数据
    std::string buf;
    std::cout<<"client say : ";
    fflush(stdout);
    std::cin>>buf;
    assert(cli_sock.Send(buf));

    buf.clear();
    assert(cli_sock.Recv(&buf));
    std::cout<<"server say : "<<buf<<std::endl;
  }
  //5、关闭套接字
  cli_sock.Close();
  
  return 0;
}
