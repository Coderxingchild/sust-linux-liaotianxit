#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>


void fun()
{
  // 有 \n 时会先打印信息再进行换行---刷新缓冲区的作用
  //printf("helloc fun~!\n");

  //没有 \n 时，不刷新缓冲区，直接退出---------不会打印该行内容
  printf("helloc fun~!");
  _exit(0);
}


int main()
{
  printf("welcom to exit2.c~!\n");

  fun();  //调用 fun 函数之后直接退出程序

  printf("break~~~\n");   //fun 中直接 exit ，因此不会执行该行代码
  return 0;
}
