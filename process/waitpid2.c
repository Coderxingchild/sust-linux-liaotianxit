#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/wait.h>

int main()
{
  //创建子进程
  pid_t cpid=fork();

  if(cpid<0){
    perror("fork error~!\n");
    exit(0);
  }
  else if(cpid==0){
    //child
    sleep(3);  //等待 3s 之后，子进程正常退出，父进程中打印正常退出的子进程信息
    
   // sleep(100);  //子进程没有正常退出，若 kill 手动删除一个子进程，则会提示异常退出信息
    exit(99);
  }

  //父进程
  
  int status;
  int  ret;
  
  while((ret=waitpid(-1,&status,WNOHANG)==0)){   //非阻塞等待任意子进程退出
    printf("子程序还没有退出~~~！\n");
    sleep(2);
  }

  if(ret<0){
    perror("fork error!\n");
    return -1;
  }

  //取出 static 中 低 7 位，判断低七位的异常信号值是否为 0 ，0-正常退出，！0-异常退出
  //退出返回值为 static 中低16位中的高八位
  
  if((status & 0x7f)==0){
    //判断 static 变量的低七位是否为0 ---- 程序是否正常退出
    printf("%d 子进程退出了！返回值是 ： %d\n",ret,(status >> 8) & 0xff);
  }
  else{
    printf("子进程异常退出！\n");
  }

  sleep(10000);
  return 0;
}
