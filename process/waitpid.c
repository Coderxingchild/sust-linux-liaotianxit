#include<stdio.h>
#include<stdlib.h>
#include<sys/wait.h>  //waitpid 的头文件
#include<unistd.h>

int main()
{
  pid_t cpid=fork();  //创建一个子进程

  if(cpid<0){
    perror("fork error~!\n");  //创建子进程失败
    exit(0);                   
  }
  else if(cpid==0){
    //child
    sleep(3);
    exit(257);
  }
  else{
    //父进程
    
    int status,ret;
   // ret=waitpid(-1,&status,0);  //此时表示等待任意一个子进程退出---阻塞

    while((ret=waitpid(-1,&status,WNOHANG))==0){   //非阻塞等待任意子进程退出
        printf("还没有子进程退出，我先去干别的事儿~~\n");
        sleep(2);
    }


    //waitpid(pid_t pid,int* status,int options)
    //pid = -1 表示等待任意一个子进程退出，当第一个参数 pid=-1时 waitpid == wait 的使用; pid>0 表示等待指定子进程退出
    //0 表示阻塞等待，WNOHANG-非阻塞等待
  
    //没有子进程退出
    if(ret<0){
      printf("childfork error~!\n");
      return -1;
    }

    //存在子进程正常退出
    printf("%d 子进程退出了~~~，退出返回值为 ： %d\n",ret,status);
  }

  return 0;
}
