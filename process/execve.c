#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>


int main()
{
  //测试
  //
  
  printf("这是我原程序的起始位置~~~\n");

  char* argv[]={"ls","-l","-a",NULL};
  char* env[]={NULL};

//测试一
   //execve 需要指定运行参数
  //execve("./argc",argv,env);
  
  //execve("/bin/ls",argv,env);
//测试二

  //execve("ls",argv,env);   //------------替换失败
  //execve("/bin/ls",argv,env);  //需要指定路径  以及  环境变量 env

//测试三
  //execvp("./argc",argv);
  //execvp("ls",argv);      //不需要指定路径 不需要设置 env

//测试四
  //execv("ls",argv);       //替换失败
   
  //execv("/bin/ls",argv);   //需要指定路径


//测试五
  // execlp("ls","-l","-a",NULL);       //不需要指定环境变量 


//测试六
   
  //execle("/bin/ls","ls","-l","-a",NULL,env);       //需要指定环境变量,需要指定位置
  printf("这是我原程序的结束位置~~~\n");

  return 0;
}













//int main()
//{
//  printf("这是我程序运行的起始位置~\n");

  //char* argv[]={"-a","-d","-c","xzz",NULL};   //argv 字符函数必须以 NULL 结尾
  //char* env[]={"MYVAL=999","CLASS=98",NULL};

 // execve("./argc",argv,env);

  
//  char *argv[]={"ls","-l",NULL};
  //char* env[]={NULL};

  //execvp("ls",argv);   //env 默认采用已有的环境变量


  //execv("ls",argv);   //不指定路径时-替换失败
  
//  execv("/bin/ls",argv);

//  perror("execv error\n");


//  printf("这是我程序运行的终止~~~\n");


//  return 0;
//}
