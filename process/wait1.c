#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/wait.h>  //wait 头文件
int main()
{
  pid_t ret =  fork();   //创建一个子进程
  
  if(ret < 0){
    perror("fork error!");   //创建子进程失败
    exit(0);
  }
  else if(ret==0){
    //子进程
    sleep(3);
    exit(99);
  }
  else{
    //父进程
    
    int status;
    //父进程等待，wait 等待任意一个子进程退出，获取它的退出返回值
    ret=wait(&status);
    //不存在子进程退出
    if(ret<0){
      perror("childfork error!\n");
      return -1;
    }

    //存在子进程退出
    printf("%d 子进程退出了~~~，返回值是：%d\n",ret,status);
  }

  return 0;
}
