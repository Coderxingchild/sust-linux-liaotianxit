#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

int main(int argc,char* argv[],char* env[])
{
  int i=0;
  for(;argv[i]!=NULL;++i)
    printf("argv[%d]=[%s]\n",i,argv[i]);

  printf("~~~~~~~~~~~~~~~~~~\n");

  i=0;
  for(;env[i]!=NULL;++i)
    printf("env[%d]=[%s]\n",i,env[i]);

  return 0;
}
