#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/wait.h>

int main()
{
  pid_t cpid=fork();
  if(cpid<0){
    perror("fork error~!\n");
    exit(0);
  }
  else if(cpid==0){
    //child
    sleep(1);
    exit(3);
  }
  //parent
  
  int status;
  //wait 的测试
  //int ret=wait(&status);  //阻塞等待任意一个子进程退出，并获取退出子进程相关信息

  //int ret=waitpid(-1,&status,0);  //阻塞等待任意一个子进程退出===>wait

  //int ret=waitpid(cpid,&status,0);  //阻塞等待指定的子进程退出
  
  //int ret=waitpid(cpid,&status,WNOHANG); //非阻塞等待指定子进程退出，若未退出则直接报错返回
  //
  
  int ret;
  //waitpid 非阻塞等待测试
  while((ret=waitpid(cpid,&status,WNOHANG))==0){
    printf("指定的子进程还未退出，再循环等待中....~!\n");
    sleep(3); 
 }

  if(ret<0){
    perror("fork error~!\n");
    return -1;
  }

  
   printf("%d 子进程退出了~~~，退出返回值为：%d\n",ret,(status>>8)&0xff);


  //if((status & 0x7f) == 0){
    //检测异常信号值是否为 0 -正常退出 
    // status 低 16 位中第七位表示异常信号值，0-正常退出，!0-异常退出
 //   printf("%d 子进程退出了~~~，退出返回值为：%d\n",ret,(status>>8)&0xff);
  //}
 // else{
 //   printf("程序异常退出了~~~~\n");
 // }


  return 0;
}
