#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>



void fun1()
{
  //含有 \n 时，打印出来会有换行
  printf("hello bit!\n");
  
  //没有 \n 时打印出来的信息没有换行
  printf("hello bit!");

  //采用库函数 exit
  exit(0);
}

int main()
{
  printf("welcom to exit1.c~!\n");

  fun1();

  printf("welcom to shutdown~!\n");
  return 0;
}


