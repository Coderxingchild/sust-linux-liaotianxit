#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/wait.h>   //waitpid 的头文件

int main()
{
  pid_t cpid=fork();

  if(cpid<0){
    perror("fork error~!\n");
    exit(0);
  }
  else if(cpid==0){
    //子进程
    
   // sleep(3);
    exit(99);
  }
  else{
    //父进程
    //
    
    int status ,ret;

   // int cpid=wait(&status1);
   // if(cpid<0){
   //   perror("error~!\n");
   //   return -1;
   // }

    ret=waitpid(cpid,&status,0);   //等待指定子进程 7288 退出，0-阻塞退出
    
    if(ret<0){
      perror("childfork error~!\n");
      return -1;
    }

    printf("%d 子进程已经退出了~~，退出返回值为：%d\n",ret,status);
  }

  return 0;
}
