#include<stdio.h>
#include<pthread.h>
#include<unistd.h>

void* thread_entry(void* arg)
{
  //线程入口函数
  int count=0;
  while(1){
    printf("i am nomal thread:%s\n",arg);
  
    count++;
    //pthread_exit(NULL);    //任意位置调用可以退出线程
    if(count==3)return "nihao ";         //返回值为字符串 类型
    
    sleep(1);
  }
  return NULL;
}

int main()
{
  //线程的创建
   //int pthread_create(pthread_t *thread, const pthread_attr_t *attr,void *(*start_routine) (void *), void *arg);

  pthread_t tid;     //用来保存线程 ID
  char* arg="leihoua!!";     //给线程入口函数传入的参数

  int ret=pthread_create(&tid,NULL,thread_entry,(void*)arg);
  if(ret!=0){
    printf("thread create error!\n");
    return -1;
  }

  //线程等待
  void* retval;   //用于接收退出返回值
  ret=pthread_join(tid,&retval);     //等待指定的线程 tid 退出，并获取退出返回值 retval  
  if(ret!=0){
    printf("pthread_join failed\n");
    return -1;
  }
  printf("exit----%s\n",retval);  //打印退出返回信息


  //pthread_cancel(tid);         //任意位置调用退出线程

   
  //普通线程一旦创建成功，创建出来的这个线程调度的是传入的线程入口函数，因此能够走下来的只有主线程
  //线程中不存在父子线程
  while(1){                      //打印线程ID
    printf("i am main thread!!----%d\n",tid);
    sleep(1);
  }
  return 0;
}
