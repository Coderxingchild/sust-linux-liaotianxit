#include<stdio.h>
#include<pthread.h>
#include<unistd.h>

int ticket=100;

void* Scalper(void* arg)
{
  pthread_mutex_t *mutex=(pthread_mutex_t*)arg;
  //入口函数
  while(1){
    pthread_mutex_lock(mutex); //进行加锁操作
    usleep(10);
    if(ticket>0){
      printf("%p 抢到了 %d 号票\n",pthread_self(),ticket);
      ticket--;
      //抢票结束进行解锁
      pthread_mutex_unlock(mutex);

    }else{
      printf("票没了~~！\n");
      pthread_mutex_unlock(mutex);   //加锁后在任意有可能退出线程的地方都需要解锁
      pthread_exit(NULL);
    }
  }
  usleep(1);
  return NULL;
}

int main()
{
  pthread_t tid[4];   //创建四个线程
  
  pthread_mutex_t mutex;   //定义互斥锁
  pthread_mutex_init(&mutex,NULL);   //互斥锁的初始化操作

  int i=0;
  for(i=0;i<4;++i){
    int ret=pthread_create(&tid[i],NULL,Scalper,&mutex);  //将锁传给入口函数参数
    if(ret!=0){
      printf("create pthread error~!\n");
      return -1;
    }
  }

  for(i=0;i<4;++i){
    pthread_join(tid[i],NULL);      //等待指定的线程退出，退出返回值 NULL
  }

  //线程运行结束释放锁
  pthread_mutex_destroy(&mutex);
  return 0;
}
