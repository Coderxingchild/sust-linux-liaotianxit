#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include <sys/wait.h>

int main()
{
  //进程创建

  printf("hello biter_create fork()!\n");

  pid_t pid=fork();

  if(pid<0){
    printf("fork error!\n");
    exit(0);
  }else if(pid==0){
    printf("i am child process!\n");
    sleep(10);
    exit(99);
  }else{
    printf("i am parent process!\n");
   
    int status;
    //pid_t ret=waitpid(-1,&status,0); //阻塞等待任意子进程退出

    pid_t ret=waitpid(pid,&status,0); //等待指定子进程退出

    if(ret<0){
      printf("there is no child quit~\n");
      exit(-1);
    }

    printf("%d child had quit,the quit number is %d\n",ret,(status>>8)&0xff);
  }

  printf("this is con_code!\n");

  //进程等待
//  if(pid>0){
//    //父进程进行等待
//    int status;
//    pid_t ret=wait(&status);
//    if(ret<0)
//    {
//      printf("wait no child!\n");
//      exit(-1);
//    }
//
//    printf("%d 子进程退出了，退出码:%d\n",ret,(status>>8)&0xff);
//  }
//  

  

  return 0;

}
