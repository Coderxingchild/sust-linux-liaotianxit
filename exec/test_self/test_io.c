#include<stdio.h>
#include<stdlib.h>
#include<string.h>


int main() 
{
  FILE* fp=fopen("./test0.txt","w+"); //可写方式打开一个文件，不存在则创建

  if(NULL==fp){
    perror("fopen error!");
    exit(-1);
  }

  const char* data="nihaoa~~";
  size_t ret = fwrite(data,1,strlen(data),fp);
  if(ret!=strlen(data)){
    perror("fwrite error!");
    fclose(fp); //写入失败关闭文件
    exit(-1);
  }


  //读取文件
  fseek(fp,0,SEEK_SET);  //从起始位置开始读取

  char buf[1024]={0};
  ret=fread(buf,1,1023,fp);
  if(ret==0){
    if(feof(fp))
      printf("read end of this file!\n");
    if(ferror(fp))
    {
      perror("fread error!");
      fclose(fp);
      return -1;
    }
  }


  fclose(fp);

  printf("read this file : %s\n",buf);

  return 0;
}
