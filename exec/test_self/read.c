#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/stat.h>
#include<unistd.h>
#include<fcntl.h>

int main()
{
  umask(0);  //设置掩码
  int fp=open("./test.txt",O_RDWR|O_CREAT|O_TRUNC,0664);
  if(fp<0){
    perror("open error!\n");
    return -1;
  }

  //打开成功，写入信息
  const char* data="i want to learn more knoledge about linux~~";
  ssize_t ret=write(fp,data,strlen(data));
  if(ret<0){
    perror("write error!\n");
    close(fp);
    return -1;
  }


  //写入成功，进行读取
  lseek(fp,0,SEEK_SET);

  char buf[1024]={0};
  ret=read(fp,buf,1023);
  if(ret<0){
    perror("read error!\n");
    close(fp);
    return -1;
  }

  printf("this file : %s\n",buf);
  close(fp);

  return 0;
}
