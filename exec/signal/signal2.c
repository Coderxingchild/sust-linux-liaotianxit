#include<stdio.h>
#include<string.h>
#include <signal.h>
#include <unistd.h>

void sigcb(int no)
{
  printf("no = %d\n",no);
}

int main()
{

  //建立两个信号中断操作
  signal(SIGINT,sigcb);        //2 号-----------非可靠信号，会被注册一次，因此只会被处理一次
  signal(SIGRTMIN+5,sigcb);  //39 号-----------可靠信号，会被注册多次  因此会被处理多次


  sigset_t set,old;
  sigemptyset(&set);
  sigemptyset(&old); //清空操作
  sigfillset(&set); //将所有信号添加到 set 集合中

  //进行信号阻塞
  sigprocmask(SIG_BLOCK,&set,&old);

  printf("按回车继续！\n");
  getchar();

  //解除阻塞
 // sigprocmask(SIG_UNBLOCK,&set,NULL);
 
  sigprocmask(SIG_SETMASK,&old,NULL);       //使用 old 集合重置


  while(1){
    printf("hello world~!\n");
    sleep(1);
  }
  return 0;
}
