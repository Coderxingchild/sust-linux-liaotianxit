#include<stdio.h>
#include<string.h>
#include <signal.h>
#include <unistd.h>

void sigcb(int no)
{
  printf("no = %d\n",no);
}

int main()
{
 // signal(SIGINT,sigcb);     //当收到 SIGINT 信号，则使用 sigcb 函数进行信号中断
    
  signal(SIGINT,SIG_IGN);  //收到 SIGINT 信号，采用忽略处理


  while(1){
    printf("hello world~!\n");
    sleep(1);
  }
  return 0;
}
