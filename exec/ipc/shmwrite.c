#include<stdio.h>
#include<unistd.h>
#include<sys/shm.h>
#include<string.h>
#include<stdlib.h>


#define SHM_KEY 0x12345678

int main()
{
  //开辟一块内存
  int shmid=shmget(SHM_KEY,4096,IPC_CREAT|0664);
  //固定大小为 4096，IPC_CREAT 内存存在则直接打开，不存在则创建打开
  
  if(shmid<0){
    perror("shmget error");
    return -1;
  }

  //进行映射
  //shmid 为共享内存的句柄，NULL 采取默认分配的虚拟地址，0-可读可写
  void* start=shmat(shmid,NULL,0);
  if(start==(void*)-1){
    perror("shmat error");
    return -1;
  }


  //写入数据
  
  int id=0; 
  while(1){
    sprintf(start,"马上就要周末了! + %d\n",id++);
    sleep(1);
  }


  //解除映射
  shmdt(start);
  //删除共享内存
  shmctl(shmid,IPC_RMID,NULL); //IPC_RMID 标记要被删除

  return 0;
}
