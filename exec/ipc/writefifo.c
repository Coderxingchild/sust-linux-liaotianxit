#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/shm.h>
#include<sys/stat.h>
#include<unistd.h>
#include<fcntl.h>
#include<errno.h>

int main()
{
  umask(0);

  //创建一个命名管道
  int ret=mkfifo("./testfifo",0664);
  if(ret<0 && errno!=EEXIST){
    perror("mkfifo error");
    return -1;
  }


  //向管道中写入数据
  int fp=open("./testfifo",O_WRONLY);  //只写打开
  if(fp<0){
    perror("openfifo error");
    return -1;
  }

  while(1){
    printf("i am process A!!\n");
    fflush(stdout);

    char str[1024]={0};
    scanf("%s",str);

    int fd =write(fp,str,strlen(str));
    if(fd<0){
      perror("write error");
      close(fp);
      return -1;
    }
  }

  close(fp);

  return 0;
}
