#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<errno.h>

int main()
{
  umask(0);

  int ret=mkfifo("test1.fifo",0664);
  if(ret<0 && errno!=EEXIST){
    perror("mkfifo error");
    return -1;
  }

  //从管道中读取数据
  int fp=open("test1.fifo",O_RDONLY);  //只读打开
  if(fp<0){
    perror("open error");
    return -1;
  }

  while(1){
    char buf[1024];
    int ret=read(fp,buf,1023);
    if(ret<0){
      perror("read error");
      close(fp);
      return -1;
    }
    else if(ret==0){
      printf("所有写端被关闭!\n");
      close(fp);
      return -1;
    }
    printf("接收到了进程信息：%s\n",buf);
  }
  close(fp);

  return 0;
}
