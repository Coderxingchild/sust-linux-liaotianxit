#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/stat.h>
#include<unistd.h>
#include<fcntl.h>
#include<errno.h>

int main()
{
  umask(0);

  //创建一个命名管道
  int ret=mkfifo("./testfifo",0664);
  if(ret<0 && errno!=EEXIST ){
    perror("mkfifo error");
    return -1;
  }


  //从管道中读取数据
  int fp=open("./testfifo",O_RDONLY);  //只读打开
  if(fp<0){
    perror("openfifo error");
    return -1;
  }

  while(1){
    //读取管道信息
    char buf[1024];
    int fd = read(fp,buf,1023);
    if(fd<0){
      perror("read error");
      close(fp);
      return -1;
    }
    else if(fd==0){
      printf("所有写端被关闭！\n");
      close(fp);
    }
    printf("接收到了另一个进程的信息：%s\n",buf);
  }

  close(fp);

  return 0;
}
