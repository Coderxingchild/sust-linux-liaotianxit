#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>

int main()
{
  int pipefd[2]={0};

  int ret=pipe(pipefd);
  if(ret<0){
    perror("pipe error");
    return -1;
  }


  //父进程进行写入
  char* str="i am father!\n";
  write(pipefd[1],str,strlen(str));


  pid_t child=fork();
  if(child==0){
    char buf[1024];
    read(pipefd[0],buf ,1023);
    printf("接收到了父进程的信息 ：%s\n",buf);
  }

  close(pipefd[0]);
  return 0;
}
