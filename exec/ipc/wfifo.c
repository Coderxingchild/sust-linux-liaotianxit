#include<stdio.h>
#include<string.h>
#include<fcntl.h>
#include<unistd.h>
#include<errno.h>
#include<sys/stat.h>

int main()
{
  umask(0); //设置该文件内部掩码
  //创建一个管道
  int ret=mkfifo("test1.fifo",0664);
  if(ret<0 && errno!=EEXIST){
    perror("mkfifo error");
    return -1;
  }



  //向管道中写入数据

  int fp=open("test1.fifo",O_WRONLY);  //只读打开
  if(fp<0){
    perror("open error");
    return -1;
  }

  while(1){
    printf("i am process A!\n");
    fflush(stdout);
    char buf[1024];
    scanf("%s",buf);
    int ret=write(fp,buf,strlen(buf));
    if(ret<0){
      perror("write error");
      close(fp);
      return -1;
    }
  }

  close(fp);
  return 0;
}
