#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<unistd.h>

void* Ca_can(void* arg)
{
  //在入口函数内进行线程分离
  pthread_detach(pthread_self());


  int count=0;
  //线程入口函数
  while(1){
    printf("I am nomal thread!\n");
    count++;
    if(count==3) return "nihao";  //入口函数中退出线程
    sleep(1);
  }
  return NULL;
}

int main()
{
  //创建一个线程  
 // int pthread_create(pthread_t *thread, const pthread_attr_t *attr,
              // void *(*start_routine) (void *), void *arg);
  
  pthread_t tid;  //用于接收线程 ID
  const char* arg = "nihaoa";   //传递给入口函数的参数信息

  int ret=pthread_create(&tid,NULL,Ca_can,(void*)arg);
  if(ret!=0){
    perror("pthread_create error!\n");
    return -1;
  }
    
  //线程分离
  // int pthread_detach(pthread_t thread);
  //pthread_detach(tid);

  //线程等待
  // int pthread_join(pthread_t thread, void **retval);
  void* retval;
  ret=pthread_join(tid,&retval);
  if(ret!=0)
  {
    perror("phread_join error!\n");
    return -1;
  }
  printf("thread exit-----%s\n",retval);  //输出退出线程的返回值





  //线程退出-----任意位置退出
  // pthread_exit(NULL);
 
  //线程创建成功
  while(1){
    printf("I am main thread!------%d\n",tid); //打印创建好的线程ID
    sleep(1);
  }

 

  
  return 0;
}
