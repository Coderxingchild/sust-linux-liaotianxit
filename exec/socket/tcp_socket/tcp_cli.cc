#include "tcp_socket.hpp"

//客户端代码实现

int main(int argc,char* argv[])
{
  if(argc!=3){
    std::cout<<"./tcp_cli 192.168.75.128 9000";
    return -1;
  }
  std::string srv_ip=argv[1];
  uint16_t srv_port=std::stoi(argv[2]);

  TcpSocket cli_socket;
  //创建套接字
  assert(cli_socket.Socket());
  //绑定地址信息----客户端不需要

  //请求连接服务端
  assert(cli_socket.Connect(srv_ip,srv_port));
  while(1){
  //循环收发数据
  //发送数据
    char tmp[1024]={0};
    fflush(stdout);
    std::cout<<"client say: ";
    std::cin>>tmp;
    assert(cli_socket.Send(tmp));
  
    std::string data;
    assert(cli_socket.Recv(&data));
    std::cout<<"server say: "<<data<<std::endl;

  }
  //关闭套接字
  cli_socket.Close();

  return 0;
}
