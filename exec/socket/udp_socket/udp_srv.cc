#include "udp_socket.hpp"
#include<iostream>
#include<assert.h>

int main(int argc,char* argv[])
{
  if(argc!=3){
    printf("./udp_srv 192.168.75.128 9000\n");
    return -1;
  }
  std::string cli_ip=argv[1];
  uint16_t cli_port=std::stoi(argv[2]);

  UdpSocket srv_sock;
  //创建套接字
  assert(srv_sock.Socket());
  //绑定地址
  assert(srv_sock.Bind(cli_ip,cli_port));
  while(1){
  //接收数据
    std::string tmp;
    assert(srv_sock.Recv(tmp,&cli_ip,&cli_port));

    std::cout<<"client: "<<cli_ip<<":"<<cli_port<<"say : "<<tmp<<std::endl;

    //发送数据
    tmp.clear();
    std::cout<<"server say : ";
    fflush(stdout);
    std::cin>>tmp;
    assert(srv_sock.Send(tmp,cli_ip,cli_port));
  }
  //关闭套接字
  srv_sock.Close();
  return 0;
}
