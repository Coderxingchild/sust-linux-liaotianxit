#include "udp_socket.hpp"
#include<iostream>
#include<assert.h>

int main(int argc,char* argv[])
{
  if(argc!=3){
    printf("./udp_cli 192.168.75.128 9000\n");
    return -1;
  }

  std::string srv_ip=argv[1];
  uint16_t srv_port=std::stoi(argv[2]);

  UdpSocket cli_sock;

  //创建套接字
  assert(cli_sock.Socket());
  //绑定地址
  //客户端不建议绑定地址空间
  
  while(1){
  //发送数据
  std::string tmp;
  std::cout<<"client say : ";
  fflush(stdout);
  std::cin>>tmp;
  assert(cli_sock.Send(tmp,srv_ip,srv_port));


  //接收数据
  std::string data;
  assert(cli_sock.Recv(data,&srv_ip,&srv_port));
  std::cout<<"server say : "<<data<<std::endl;
  
  }
  //关闭套接字
  cli_sock.Close();
  return 0;
}
