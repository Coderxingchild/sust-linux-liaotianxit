#include<stdio.h>
#include<unistd.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<string.h>

int main()
{
  FILE*fp=fopen("./fp.txt","w+");
  if(fp==NULL){
    perror("fopen error");
    return -1;
  }

  //打开文件成功
  
  char *str="linux so easy!\n";
  size_t ret=fwrite(str,1,strlen(str),fp);

  if(ret!=strlen(str)){
    //说明写入失败
    perror("fwrite error");
    fclose(fp);
    return -1;
  }



  //写入文件成功，进行读取数据
  //
  

  //将文件定位到起始位置
  
  fseek(fp,0,SEEK_SET);


  char buf[1024] = {0};
  ret=fread(buf,1,1023,fp);
  if(ret==0){
    if(feof(fp)){
      printf("end of this file");
    }
    if(ferror(fp)){
      perror("fread error");
      fclose(fp);
      return -1;
    }
  }


  printf("%s\n",buf);
  fclose(fp);

  return 0;
}

