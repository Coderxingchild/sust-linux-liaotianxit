
#include<stdio.h>
#include<unistd.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<string.h>

//open 的应用

int main()
{

  int fp=open("fp1.txt",O_RDWR|O_CREAT|O_TRUNC,0664); //清空写入
  if(fp<0){
    perror("open error");
    return -1;
  }

  //打开文件成功，写入数据
  
  char *str="i like linux!\n";

  ssize_t ret=write(fp,str,strlen(str));
  if(ret<0)
  {
    perror("write error");
    close(fp);
    return -1;
  }

  //写入成功，进行数据读取
  //
  
  lseek(fp,0,SEEK_SET); //从文件起始开始读取


  char buf[1024]={0};
  ret=read(fp,buf,1023);
  if(ret<0){
    perror("read error");
    close(fp);
    return -1;
  }

  printf("%s\n",buf);   //读取文件数据

  close(fp);

  return 0;
}
