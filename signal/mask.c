#include<stdio.h>
#include<signal.h>
#include<unistd.h>
#include<stdlib.h>

void sigcb(int no)
{
  printf("no = %d\n",no);
}

int main()
{
  signal(SIGINT,sigcb);   //修改 2 号信号处理方式-----------非可靠信号，多次收到但只会注册一次
  signal(SIGRTMIN+5,sigcb);  //修改 39 号信号的处理方式----------可靠信号，多次收到就会注册多次

  sigset_t set,old;
  sigemptyset(&set);  //清空集合
  sigemptyset(&old);
  sigfillset(&set);  //将所有信号填充到 set 集合

  sigprocmask(SIG_BLOCK,&set,&old);  //信号阻塞
  printf("按回车继续运行！\n");
  getchar();

  sigprocmask(SIG_UNBLOCK,&set,NULL);   //解除阻塞
 // sigprocmask(SIG_SETMASK,&old,NULL); //还原旧信号

  while(1)
    sleep(1);
  return 0;
}
