#include<stdio.h>
#include<unistd.h>
#include <signal.h>
#include <stdlib.h>

int main()
{
  //kill(getpid(),SIGTERM);    //给当前进程一个指定的信号 SIGTERM

  //raise(SIGTERM);

  abort();     //给当前进程自身发送一个 SIGABRT 信号

  while(1){
    sleep(1);
  }
  return 0;
}
