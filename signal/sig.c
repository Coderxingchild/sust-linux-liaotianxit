#include<stdio.h>
#include<signal.h>
#include<unistd.h>
#include<stdlib.h>

void sigcb(int no){
  printf("no=%d\n",no);
}
int main()
{
  signal(SIGINT,sigcb);

  sigset_t set,old;
  sigemptyset(&set);  //清空集合
  sigemptyset(&old);

  sigfillset(&set);  //填充集合
  
  sigprocmask(SIG_BLOCK,&set,&old);   //阻塞

  printf("按回车，继续运行！\n");
  getchar();

  sigprocmask(SIG_SETMASK,&old,NULL);   //使用 old 还原
 // sigprocmask(SIG_UNBLOCK,&set,NULL);  //解除阻塞
  while(1){
    sleep(1);
  }
  return 0;
}
