#include<stdio.h>
#include<signal.h>
#include<stdlib.h>
#include<unistd.h>

void sigcb(int no)
{
  printf("no=%d\n",no);
}

int main()
{
  //signal(SIGINT,sigcb); 
  
  signal(SIGINT,SIG_IGN);   //忽略中断操作
  
  while(1){
    printf("signal!\n");
    sleep(1);
  }

  //kill(getpid(),SIGTERM);  //给当前进程发送一个 SIGTERM 信号
  //while(1){
  //  sleep(1);
 // }
  return 0;
}
