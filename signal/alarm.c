#include <stdio.h>
#include<unistd.h>
#include <signal.h>

void mysleep(int n)
{
  alarm(n);
  pause();
}

void sigcb()
{
  printf("3s 已经结束！\n");
}

int main()
{
  signal(SIGALRM,sigcb);  //信号中断
  mysleep(3);    //进行 alarm 

  while(1){
    printf("use alarm!\n");
    sleep(5);
  }

  return 0;
}
