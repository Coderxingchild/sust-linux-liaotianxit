#include<stdio.h>
#include<signal.h>
#include<stdlib.h>
#include<unistd.h>

int a=1,b=1;
void test(char* str)
{
  printf("%s----start-------\n",str);
  a++;
  b++;
  printf("%s-%d\n",str,a+b);
 
  printf("%s-----end-------\n",str);
}
void sigcb(int no)
{
  test("sigcb");
}
int main()
{
  signal(SIGINT,sigcb);
  test("main");
  sleep(2);
  return 0;
}
