#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<fcntl.h>
#include<string.h>
#include<sys/stat.h>
#include<errno.h>     //当要创建的命名文件已经存在时


int main()
{
  
  umask(0);  //设置当前进程的文件权限掩码为 0
 
  //创建一个命名管道，管道名 test.fifo
  int ret=mkfifo("./test.fifo",0664);
  if(ret<0 && errno!=EEXIST){    //errno 是一个全局变量，每个系统调用接口使用返回之前都会重置自己的错误编号
    perror("mkfifo error");
    return -1;
  }

  //打开管道并向管道读取数据
  int fp=open("./test.fifo",O_RDONLY);
  if(fp<0){
    perror("open error");
    return -1;
  }

  while(1){
    char buf[1024]={0};

    //向管道写入数据
    int ret=read(fp,buf,1023);
    if(ret<0){
      perror("read error");
      close(fp);
      return -1;
    }
    else if(ret==0){
      printf("所有写端被关闭！\n");
      close(fp);
      return -1;
    }

    printf("小明：%s\n",buf);
  }

  close(fp);
  return 0;
}

