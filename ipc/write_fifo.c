#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<fcntl.h>
#include<string.h>
#include<sys/stat.h>
#include<errno.h>     //当要创建的命名文件已经存在时


int main()
{
  //向命名管道中写入数据
  
  umask(0);  //设置当前进程的文件权限掩码为 0
 
  //创建一个命名管道，管道名 test.fifo
  int ret=mkfifo("./test.fifo",0664);
  if(ret<0 && errno!=EEXIST){    //errno 是一个全局变量，每个系统调用接口使用返回之前都会重置自己的错误编号
    perror("mkfifo error");
    return -1;
  }

  //打开管道并向管道写入数据
  int fp=open("./test.fifo",O_WRONLY);
  if(fp<0){
    perror("open error");
    return -1;
  }


  while(1){
    printf("小明：");
    fflush(stdout);
    char buf[1024]={0};
    scanf("%s",buf);    //从键盘写入字符信息

    //向管道写入数据
    int ret=write(fp,buf,strlen(buf));

    if(ret<0){
      perror("write error");
      close(fp);
      return -1;
    }
  }

  close(fp);
  return 0;
}

