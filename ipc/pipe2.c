#include<stdio.h>
#include<string.h>
#include<sys/wait.h>
#include<unistd.h>
#include<stdlib.h>


int main()
{
  int pipefd[2]={0};
  int ret=pipe(pipefd);
  if(ret<0){
    perror("pipe error");
    return -1;
  }


  pid_t pid1=fork();
  if(pid1==0){
    //子进程1 ，进行数据的写入

    close(pipefd[0]);//只进行写入数据，不进行读取
    printf("write start!\n");
    int total=0;
    //循环写入数据，但只读取一次
    char* str="好好学习Linux，不然没工作!\n";
    int ret=  write(pipefd[1],str,strlen(str));
    total+=ret;
    printf("已经写入 %d 字节的数据了！\n");
    exit(0);
  }


  pid_t pid2=fork();
  if(pid2==0){
    //子进程2，进行数据的读取
    sleep(2);
    close(pipefd[1]);

    while(1){
      printf("子进程2已经睡醒了，开始读取数据！\n");
      char buf[1024];
      int ret = read(pipefd[0],buf,1023);
      if(ret==0){
        printf("写端被关闭!\n");
      }
     // printf("接收到了警告：%s\n",buf);
    }
    exit(0);
  }

  close(pipefd[0]);
  close(pipefd[1]);
  //父进程
  wait(NULL);
  wait(NULL);

  return 0;
}
