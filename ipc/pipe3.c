#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/wait.h>

int main()
{
  int pipefd[2]={0};
  int ret=pipe(pipefd);
  if(ret<0){
    perror("pipe error");
    return -1;
  }

  pid_t ps_child=fork();
  if(ps_child==0){
    close(pipefd[0]);
    dup2(pipefd[1],1);
    execlp("ps","ps","-ef",NULL);
    exit(0);
  }

  pid_t grep_child=fork();
  if(grep_child==0){
    close(pipefd[1]);
    dup2(pipefd[0],0);
    execlp("grep","grep","pipe",NULL);
    exit(0);
  }


  close(pipefd[0]);
  close(pipefd[1]);

  waitpid(ps_child,NULL,0);
  waitpid(grep_child,NULL,0);

  return 0;
}
