#include<stdio.h>
#include<unistd.h>
#include <sys/shm.h>
#include<string.h>
#define FLAG 0x11223344

int main()
{
  //创建共享内存
  // int shmget(key_t key, size_t size, int shmflg);
  int shmid=shmget(FLAG,4096,IPC_CREAT|0664);
  if(shmid<0){
    perror("shmget error");
    return -1;
  }
  //建立映射关系
  //void *shmat(int shmid, const void *shmaddr, int shmflg);
  void* start=shmat(shmid,NULL,0);     //NULL--映射首地址默认采用系统自动分配，0---可读可写
  if(start==(void*)(-1))
  {
    perror("shmat error");
    return -1;
  }
  //向共享内存中写入数据
  while(1){
    sprintf(start,"hello,I am process A!\n");
    sleep(3); 
  }

  //解除映射关系
  //int shmdt(const void *shmaddr);
  shmdt(start);
  //删除共享内存
  // int shmctl(int shmid, int cmd, struct shmid_ds *buf);
  shmctl(shmid,IPC_RMID,NULL);
  return 0;
}


