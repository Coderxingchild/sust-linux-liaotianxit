#include<stdio.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<string.h>
#include<unistd.h>

int main()
{
  int pipefd[2];
  int ret=pipe(pipefd); //创建一个管道

  if(ret<0){
    perror("pipe error");
    return -1;
  }


  int pid1=fork();
  if(pid1==0){

    //close(pipefd[0]);   //关闭读端

    //创建一个子进程1
    //pipefd[0]读取      pipefd[1]写入
    //让子进程1写入数据，子进程2来读取

    //close(pipefd[1]); //关闭写端
    //exit(0);

    printf("write start!\n");

    while(1){
      char *data="好好学习Linux，否则没有工作！\n";
      int ret = write(pipefd[1],data,strlen(data));

      if(ret==0){
        //写入到的数据为 0 ，表示当前没有数据被读取了，故没必要在进行写入数据了
        printf("write over!\n");
      }
    }
    exit(0);
  }


  int pid2=fork();
  if(pid2==0){
    //子进程2
    //读取数据

   // close(pipefd[1]);   //关闭写端

    int buf[1024];
    while(1){
      sleep(1);
     int ret =  read(pipefd[0],buf,1023);
     if(ret==0)
       printf("没有数据读取！\n");
     // printf("收到了老大的警告: %s\n",buf);
    }
    exit(0);
  }

  //从父进程中关闭读端

  //close(pipefd[0]);   //关闭读端
  //close(pipefd[1]);  //关闭写端

  wait(NULL); //等待任意子进程退出
  wait(NULL);   //等待任意子进程退出，只有当所有子进程都退出之后，父进程才能退出

  return 0;
}
