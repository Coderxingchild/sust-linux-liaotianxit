#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/stat.h>
#include<string.h>

int main()
{
  int pipefd[2];
  int ret=pipe(pipefd);  //创建管道
  if(ret<0){
    perror("pipe error");
    return -1;
  }

  //管道创建成功，创建一个子进程1来写入数据，子进程2来读取数据
  //pipefd[0]  读取，pipefd[1]  写入

  pid_t cpid1=fork();
  if(cpid1==0){
    //子进程1
    close(pipefd[0]);
    close(pipefd[1]); 
    exit(0);

    char *str="我要好好学习linux ~ ！\n";
    write(pipefd[1],str,strlen(str));



    //int total=0;
    // while(1){
    // char *str="我要好好学习linux ~ ！\n";
    // int ret = write(pipefd[1],str,strlen(str));
    // total+=ret;
    //printf("已经写入 %d 个字节数据\n",total);      //么有数据读取时，一直写入数据会发生写入阻塞
    // }
    exit(0);
  }



  pid_t cpid2=fork();
  if(cpid2==0){
    //子进程2
    close(pipefd[1]);
    // close(pipefd[0]);
    // exit(0);         //不进行读取数据，之间退出

    int buf[1024];
    int ret = read(pipefd[0],buf,1023);
    if(ret==0){
      printf("没有读取到数据!\n");
      // printf("不好好学习没有工作！！ %s\n",buf);
    }
    exit(0);
  }


  //父进程
  close(pipefd[0]);
  close(pipefd[1]);  //父进程中关闭读写

  return 0;
}
