#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>



//库函数 fopen fclose fseek fwrite 测试
int main()
{
  FILE*fp=fopen("./test1.txt","w+");
  if(fp==NULL){
    perror("fopen error");
    return -1;
  }

  //打开成功
  //
 const char *str="这是一个测试IO操作的文件\n";
  size_t ret=fwrite(str,1,strlen(str),fp);
  if(ret!=strlen(str)){
    perror("fwrite error");
    fclose(fp);
    return -1;
  }


  fseek(fp,0,SEEK_SET);     //将文件设置到起始位置进行读取


  //读取数据
  //
  char buf[1024];
  ret = fread(buf,1,1023,fp);
  if(ret==0){
    if(feof(fp))
      printf("end of this file!\n");
    if(ferror(fp)){
      perror("fread error");
      fclose(fp);
      return -1;
      }
  }



  printf("%s",buf);    //读取数据
  return 0;
}

