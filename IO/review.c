#include<stdio.h>
#include<string.h>

int main()
{
  FILE* fp=fopen("./test.txt","w+");
  if(NULL==fp){
    perror("fopen error");
    return -1;
   }

  char *data="天黑了!\n";
  //fwrite(数据地址，块大小，块个数，句柄)
  size_t ret=fwrite(data,1,strlen(data),fp);   //返回实际写入的完整块个数

  //其实就是写入的字节长度，因为块大小为 1


  //如果写入的内容大小与原本要写入的数据大小不一样，说明写入失败
  if(ret!=strlen(data)){
    perror("fwrite error!");
    fclose(fp);
    return -1;
  }


  fseek(fp,0,SEEK_SET);  //从文件起始位置开始跳转


  char buf[1024]={0};
  ret=fread(buf,1,1023,fp);  //块大小 1，块个数=想要读取的长度，这样返回值就是实际读取到的字节长度
  if(ret==0){
   if(feof(fp))    //判断是否读到文件末尾
      printf("读取到了文件末尾，end of file");
    if(ferror(fp))    //判断上一次对 fp 的操作是否出错
    {
      perror("fread error");
      fclose(fp);
      return -1;
    }   
  }
  
  printf("%s\n",buf);
  fclose(fp);

  return 0;
}
