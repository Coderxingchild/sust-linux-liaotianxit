#include<stdio.h>
#include<string.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>

int main()
{
  umask(0);      //采用默认设置的权限值，只在当前文件内有效
  int fp=open("./tmp.txt",O_RDWR|O_CREAT|O_TRUNC,0777);
  if(fp<0){
    perror("open error");
    return -1;
  }  

  //打开成功
  
  const char *str="hello world!\n";
  ssize_t ret=write(fp,str,strlen(str));
  if(ret<0){
    perror("write error");
    close(fp);
    return -1;
  }

  //写入数据之后，文件标识到达文件末尾
  lseek(fp,0,SEEK_SET); //起始位置开始


  //写入成功，读取数据
  char buf[1024];
  ret=read(fp,buf,strlen(str));

  if(ret<0){
      perror("read error");
      close(fp);
      return -1;
  }
  printf("%s",buf);
  close(fp);
  return 0;
}
