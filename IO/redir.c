#include<stdio.h>
#include<unistd.h>
#include<fcntl.h>
#include<fcntl.h>

int main()
{
  close(1);         //1-标准输出
  int fp=open("tmp.txt",O_RDWR|O_CREAT|O_TRUNC);
  if(fp<0){
    perror("open error");
    return -1;
  }

  //打开文件成功
  printf("fp = %d\n",fp);
  
  //fflush(stdout);       //刷新文件输出缓冲区


  close(fp);

  return 0;
}
