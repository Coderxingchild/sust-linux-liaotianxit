#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/wait.h>
#include<fcntl.h>        //open
#include<sys/stat.h>   //open


int main()
{
  while(1){
    printf("【user@host~ 】$ ");
    fflush(stdout);    //刷新缓冲区

    char cmd[32]={0};            //用于存储键盘输入的字符串
    fgets(cmd,1023,stdin) ;      //获取键盘输入的字符

    cmd[strlen(cmd)-1]='\0';   //将键盘输入的字符后加上 \0 表示结束

    printf("[%s]\n",cmd);   //打印输入的字符内容

    int argc=0;
    char* argv[32]={NULL};    //用于存储分割后的字符内容
    char* ptr=cmd;   //保存获取到的字符信息
    argv[argc++]=strtok(cmd," ");        //以空格对字符串进行分割处理


    while((argv[argc]=strtok(NULL," "))!=NULL){  //strtok 第一个参数 NULL ，表示默认以上一次终止位置作为新分割的起始位置进行分割
      printf("[%s]\n",argv[argc]);
      argc++;
    }

//打印分割后的字符内容
    int i=0;
    for(;argv[i]!=NULL;++i){
      printf("[%s]\n",argv[i]);
    }
//如果当前输入的第一个字符为 cd，则更改工作路径
    if(strcmp(argv[0],"cd")==0){
      chdir(argv[1]);      //更改当前工作路径
      continue;
    }


      
    //判断是否存在重定向符号
    // ls -l -a > a.txt              //默认重定向符号之后有个空格
    //
    int redir_flag=0;            //0-没有重定向，1-清空重定向，2-追加重定向
    char *redir_file=NULL;   //标记文件
    for(i=0;i<argc;++i){
      if(strcmp(argv[i],">")==0){
        //清空重定向
        redir_flag=1;
        argv[i]=NULL; 
        break;          //重定向之后的内容为文件名，而不是指令内容
        redir_file=argv[i+1];
      }
      else if(strcmp(argv[i],">>")==0){
        //追加重定向
        redir_flag=2;
        argv[i]=NULL;
        break;
        
        redir_file=argv[i+1];
      }
    }



    //创建子进程
    pid_t cpid=fork();
    if(cpid<0){
      perror("fork error!");
      continue;
    }
    else if(cpid==0){
      //子进程实现程序替换
      //
    

      if(redir_flag==1){
        //清空重定向
        int fp = open("tmp.txt",O_RDWR|O_CREAT|O_TRUNC,0664);    //清空打开文件
        dup2(fp,1);
      }
      else if(redir_flag==2){
        //追加重定向
        int fp=open("tmp.txt",O_RDWR|O_CREAT|O_APPEND,0664);
        dup2(fp,1);

      }


      execvp(argv[0],argv);
      perror("execvp perror");
      exit(-1);  //只有程序替换失败才会执行这行代码

    }
    wait(NULL); //每一个子进程运行完毕后，才能开始捕捉下一个输入进行操作
  }

  return 0;
}


