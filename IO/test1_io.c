#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/stat.h>   //open 头文件
#include<fcntl.h>

int main()
{
  umask(0);
  int fp=open("./tmp1.txt",O_RDWR|O_CREAT|O_TRUNC,0777);
  if(fp<0){
    printf("open error");
    close(fp);
    return -1;
  }


  //读取成功，写入文件
  
  char *str="这是一个测试系统调用接口 IO 的文件\n";
  ssize_t ret = write(fp,str,strlen(str));
  if(ret<0){
    perror("write error");
    close(fp);
    return -1;
  }

  lseek(fp,0,SEEK_SET);

  //读取文件内容
  char buf[1024];
  ret=read(fp,buf,1023);
  if(ret<0){
    perror("fread error");
    close(fp);
    return -1;
  }


  printf("%s",buf);
  return 0;
}


