#include "udp_socket.hpp"


//客户端信息
//

int main(int argc,char* argv[])
{
  if(argc!=3){
    std::cout<<"usage:./udp_cli 192.168.2.2 9000\n";
    return -1;
  }

  std::string srv_ip=argv[1];
  uint16_t srv_port=std::stoi(argv[2]);

  UdpSocket cli_sock;
  //1、创建套接字
  assert(cli_sock.Socket()==true);
  //2、绑定地址信息（客户端不需要绑定）

  //3、发送信息
  while(1){
    std::string data;
    std::cout<<"client say:";
    fflush(stdout);

    std::cin>>data;
    assert(cli_sock.Send(data,srv_ip,srv_port)==true);

    //4、接收信息
    data.clear();
    assert(cli_sock.Recv(data,&srv_ip,&srv_port)==true);
    std::cout<<"server say:"<<data<<std::endl;
  }
  //5、关闭套接字
  cli_sock.Close();

  return 0;
}


