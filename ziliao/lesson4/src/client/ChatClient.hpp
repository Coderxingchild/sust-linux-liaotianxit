#pragma once
#include<stdio.h>
#include "../connectinfo.hpp"

#define CHECK_RET(p) if(p < 0) {return 0;}

class chatCli
{
  public:
    chatCli()
    {
      tcp_sockfd_ = -1;
    }
    ~chatCli()
    {

    }
    int InitCliSvr()
    {
      tcp_sockfd_=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);  //创建套接字
      CHECK_RET(tcp_sockfd_);

      struct sockaddr_in dest_addr;
      dest_addr.sin_family=AF_INET;
      dest_addr.sin_port=htons(TCPPORT);
      dest_addr.sin_addr.s_addr=inet_addr("192.168.75.128");

      CHECK_RET(connect(tcp_sockfd_,(struct sockaddr*)&dest_addr,sizeof(dest_addr)));

      char type = Register;
      send(tcp_sockfd_,&type,1,0);

      struct RegInfo ri;
      strcpy(ri.nickname_,"liu");
      strcpy(ri.school_,"SUST");
      strcpy(ri.passwd_,"123456");
      send(tcp_sockfd_,&ri,sizeof(ri),0);

      struct RespInfo rsp;
      recv(tcp_sockfd_,&rsp,sizeof(rsp),0);
      printf("status: %d,userid:%ld\n",rsp.status_,rsp.userid_);

    }


  private:
    //tcp 套接字的描述符
    int tcp_sockfd_;
};
