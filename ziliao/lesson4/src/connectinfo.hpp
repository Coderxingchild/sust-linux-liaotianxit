#pragma once
#include<stdio.h>
#include<unistd.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>

#include<iostream>
#include<cstring>

/*
 *约定双方发送的数据格式的头文件
 *
 *
 *
 * 注册：
 * 昵称：nickname
 * 学校：school
 * 密码：passwd
 *定长数据结构
 * */

#define NICKNAMELEN 20   //昵称长度规定为20字节
#define SCHOOLLEN 20
#define PASSWDLEN 20

#define TCPPORT 19898

enum ResquestType
{
  Register = 0,
  Login
};
struct RegInfo
{
  RegInfo()
  {
    memset(nickname_,'\0',sizeof(nickname_));
  }

  //成员变量
  char nickname_[NICKNAMELEN];
  char school_[SCHOOLLEN];
  char passwd_[PASSWDLEN];
};

/*
 *登录：
 *用户id ：userid
 *密码：passwd_
 *
 *
 * */

struct LoginInfo
{
  LoginInfo()
  {
    //规定 0 是非法用户 id
    
    userid_ = 0;
    memset(passwd_,'\0',sizeof(passwd_));
  }


  uint32_t userid_;
  char passwd_[PASSWDLEN];
};


/*
 *响应结构体
 *状态：
 *{注册成功，注册失败，登陆成功，登陆失败}
 *用户id:userid
 *
 * */


struct RespInfo
{
  int status_;
  uint32_t userid_;
};

/*
 *响应状态做出约定
 *
 * */

enum ResStatus
{
  RegisterFailed = 0,
  RegisterSuccess,
  LoginFailed,
  LoginSuccess
};




