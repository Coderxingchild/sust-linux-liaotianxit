/*================================================================
 *   Copyright (C) 2021 Sangfor Ltd. All rights reserved.
 *   
 *   文件名称：ChatServer.hpp
 *   创 建 者：wudu
 *   创建日期：2021年07月08日
 *   描    述：
 *
 ================================================================*/
#pragma once
#include <iostream>
#include "../connectinfo.hpp"
#include "UserManager.hpp"
/*
 * 定义chat服务端的类
 * */

class TcpNewConnect
{
    public:
        TcpNewConnect(int newsockfd)
        {
            new_sock_ = newsockfd;
        }

        ~TcpNewConnect()
        {

        }

        int GetNewSockfd()
        {
            return new_sock_;
        }

        void SetThisPointer(void* chatsvr)
        {
            server_ = chatsvr; 
        }

        void* GetThisPointer()
        {
            return server_;
        }
    private:
        int new_sock_;

        //保存charsvr类的this指针
        void* server_;
};


class ChatSvr
{
    public:
        ChatSvr()
        {
            tcp_sockfd_ = -1;
            tcp_port_ = TCPPORT;
            um_ = NULL;
        }

        ~ChatSvr()
        {
            if(tcp_sockfd_ >= 0)
            {
                close(tcp_sockfd_);
            }
        }

        /*
         * 初始化接口
         * */

        int InitSvr()
        {

            um_ = new UserManager();
            if(um_ == NULL)
            {
                printf("初始化用户管理失败\n");
                return -1;
            }
            /*
             * 1.初始化tcp的socket编程
             *    socket, bind， listen
             * */
            tcp_sockfd_ = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
            if(tcp_sockfd_ < 0)
            {
                perror("socket");
                return -1;
            }

            struct sockaddr_in addr;
            addr.sin_family = AF_INET;
            addr.sin_port = htons(tcp_port_);
            addr.sin_addr.s_addr = inet_addr("0.0.0.0");

            int ret = bind(tcp_sockfd_, (struct sockaddr*)&addr, sizeof(addr));
            if(ret < 0)
            {
                perror("bind");
                return -2;
            }

            ret = listen(tcp_sockfd_, 5);
            if(ret < 0)
            {
                perror("listen");
                return -3;
            }

            return 0;
        }

        int StartThread()
        {
            /*
             * 1.接收tcp新连接
             *    创建线程, 为tcp客户端继续服务
             * */

            while(1)
            {
                struct sockaddr_in peer_addr;
                socklen_t peer_addrlen = sizeof(peer_addr);
                int newsockfd = accept(tcp_sockfd_, (struct sockaddr*)&peer_addr, &peer_addrlen);
                if(newsockfd < 0)
                {
                    //继续
                    perror("accept");
                    printf("continue accept...\n");
                    continue;
                }

                /*
                 * 为新的客户端连接创建线程来进行处理
                 * */

                /*
                 * 如何把newsockfd传递给线程的入口函数
                 * */
                TcpNewConnect* tnc = new TcpNewConnect(newsockfd);
                if(tnc == NULL)
                {
                    close(newsockfd);
                    continue;
                }

                tnc->SetThisPointer((void*)this);

                pthread_t tid;
                int ret = pthread_create(&tid, NULL, RegiAndLoginStart, (void*)tnc);
                if(ret < 0)
                {
                    delete tnc;
                    close(newsockfd);
                    continue;
                }
            }
        }

        static void* RegiAndLoginStart(void* arg)
        {
            pthread_detach(pthread_self());

            TcpNewConnect* tnc = (TcpNewConnect*)arg;
            ChatSvr* cs = (ChatSvr*)(tnc->GetThisPointer());

            int newsockfd = tnc->GetNewSockfd();

            char type = -1;
            while(1)
            {
                ssize_t recv_size = recv(newsockfd, &type, 1, 0);
                if(recv_size < 0)
                {
                    continue;
                }
                else if(recv_size == 0)
                {
                    close(newsockfd);
                    delete tnc;
                    pthread_exit(NULL);
                }
                else
                {
                    break;
                }
            }

            int ret = -100;
            uint32_t user_id;
            switch(type)
            {
                case Register:
                    {
                        ret = cs->DealRegister(newsockfd, &user_id);
                        break;
                    }
                case Login:
                    {
                        ret = cs->DealLogin(newsockfd, &user_id);
                        break;
                    }
                default:
                    break;
                    //如何给客户端回复应答
            }

            struct RespInfo rsp;
            rsp.status_ = ret;
            if(ret == RegisterFailed)
            {
                rsp.userid_ = 0;
            }
            else
            {
                rsp.userid_ = user_id;
            }

//            if(ret == RegisterFailed)
//            {
//                rsp.status_ = RegisterFailed;
//                rsp.userid_ = 0;
//            }
//            else if(ret == RegisterSuccess)
//            {
//                rsp.status_ = RegisterSuccess;
//                rsp.userid_ = user_id;
//            }
//            else if(ret == LoginFailed)
//            {
//                rsp.status_ = LoginFailed;
//                rsp.userid_ = user_id;
//            }
//            else if(ret == LoginSuccess)
//            {
//                rsp.status_ = LoginSuccess;
//                rsp.userid_ = user_id;
//            }
//
            send(newsockfd, &rsp, sizeof(rsp), 0);

            return NULL;
        }


        int DealRegister(int newsockfd, uint32_t* user_id)
        {
            struct RegInfo ri;
            while(1)
            {
                ssize_t recv_size = recv(newsockfd, &ri, sizeof(ri), 0);
                if(recv_size < 0)
                {
                    continue;
                }
                else if(recv_size == 0)
                {
                    close(newsockfd);
                    return -2;
                }
                else
                {
                    break;
                }
            }

            int ret = um_->RegisterUserInfo(ri, user_id);
            if(ret < 0)
            {
                return RegisterFailed;
            }

            return RegisterSuccess;
        }


        int DealLogin(int newsockfd, uint32_t* userid)
        {
            struct LoginInfo li;
            while(1)
            {
                ssize_t recv_size = recv(newsockfd, &li, sizeof(li), 0);
                if(recv_size < 0)
                {
                    continue;
                }
                else if(recv_size == 0)
                {
                    close(newsockfd);
                    return -2;
                }
                else
                {
                    break;
                }
            }

           *userid = li.userid_; 
            /*
             * 需要对登录信息进行验证操作
             * 结合用户管理模块当中对应的
             * 用户信息， 进行校验
             * */
            int ret = um_->LoginCheck(li);
            if(ret < 0)
            {
                return LoginFailed;
            }
            return LoginSuccess;
        }

    private:
        int tcp_sockfd_;
        uint16_t tcp_port_;


        //用户管理模块
        UserManager* um_;
};
