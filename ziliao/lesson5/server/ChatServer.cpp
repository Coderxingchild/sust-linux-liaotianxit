/*================================================================
*   Copyright (C) 2021 Sangfor Ltd. All rights reserved.
*   
*   文件名称：ChatServer.cpp
*   创 建 者：wudu
*   创建日期：2021年07月08日
*   描    述：
*
================================================================*/

#include "ChatServer.hpp"

int main()
{
    ChatSvr cs;
    cs.InitSvr();
    cs.StartThread();


    return 0;
}
