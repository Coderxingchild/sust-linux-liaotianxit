#include<stdio.h>
#include<unistd.h>  // 系统调用接口需包含的头文件
#include<pthread.h>  // 线程需包含的头文件

void* MyThreadStart(void* arg)
{//线程入口地址  工作线程
  
  // pthread_exit(NULL);  //进程终止---谁用谁退出
  while(1)
  {
    printf("i am work thread\n");
    sleep(1);
  }
}
int main()
{
  // 主线程
  pthread_t tid;
  int ret = pthread_create(&tid,NULL,MyThreadStart,NULL);  // 创建一个线程
  if(ret<0)
  {
    perror("pthread_create");   //创建线程失败则显示错误信息
    return 0;
  }
  
  // pthread_join(tid,NULL);  //线程等待
  
  while(1)
  {
    printf("i am main thread\n");
    sleep(1);
  }
  return 0;
}
